package tableDemo.tableDemoApp;


import java.net.URISyntaxException;
import java.security.InvalidKeyException;

import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.table.CloudTable;
import com.microsoft.azure.storage.table.CloudTableClient;
import com.microsoft.azure.storage.table.TableOperation;
import com.microsoft.azure.storage.table.TableQuery;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class AzureApp extends Application
{
	
    private static final int FONT_SIZE_20 = 20;
    private static final String ARIAL_FONT = "Arial";
    
    /* *************************************************************************************************************************
	* Instructions: Start an Azure storage emulator, such as Azurite, before running the app.
	*    Alternatively, remove the "UseDevelopmentStorage=true;"; string and uncomment the 3 commented lines.
	*    Then, update the storageConnectionString variable with your AccountName and Key and run the sample.
	* *************************************************************************************************************************
	*/
	public static final String storageConnectionString = "UseDevelopmentStorage=true;";
	//"DefaultEndpointsProtocol=https;" +
	//"AccountName=<account-name>;" +
	//"AccountKey=<account-key>";

	public static void main( String[] args )
	{
	    launch();
	}
	
	private CloudStorageAccount storageAccount;
	private CloudTableClient tableClient = null;
	private CloudTable table = null;
	
	@Override
    public void start(final Stage stage) throws InvalidKeyException, URISyntaxException {
        System.out.println("Azure Table storage demo");

        // Parse the connection string and create a blob client to interact with Blob
        // storage
        storageAccount = CloudStorageAccount.parse(storageConnectionString);
        
        tableClient = storageAccount.createCloudTableClient();
    
        Group root = new Group();
        Scene scene = new Scene(root, 640, 840);
        
        GridPane gridpane = new GridPane();
        gridpane.setPadding(new Insets(5));
        gridpane.setHgap(10);
        gridpane.setVgap(10);
        
        Label label = new Label("Azure Table App");
        label.setFont(new Font("Arial Bold", 25));
        GridPane.setHalignment(label, HPos.CENTER);
        gridpane.add(label, 0, 0);
        
        createTableForm(gridpane);
        
        addDataForm(gridpane);
        
        listTableContent(gridpane);

        root.getChildren().add(gridpane);
        
        stage.setScene(scene);
        stage.show();
    }
	
	private void createTableForm(GridPane gridpane) {
        Label label2 = new Label("Enter Azure Table name:");
        label2.setFont(new Font(ARIAL_FONT, FONT_SIZE_20));
        
        final TextField tableName = new TextField ();
        HBox hb = new HBox();
        
        hb.getChildren().addAll(label2, tableName);
        hb.setSpacing(10);
        gridpane.add(hb, 0, 10);
        
        Button submit = new Button("Click to create");
        GridPane.setConstraints(submit, 1, 10);
        gridpane.getChildren().add(submit);
        
        submit.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                if ((tableName.getText() != null && !tableName.getText().isEmpty())) {
                    try {
                        createTable(tableName.getText());
                    } catch (URISyntaxException e1) {
                        e1.printStackTrace();
                    } catch (StorageException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });
    }
    
    //Create Azure Table
    private void createTable(String tableName) throws URISyntaxException, StorageException {
        table = tableClient.getTableReference(tableName);

        // Create the table if it does not exist with public access.
        System.out.println("Creating table: " + table.getName());
        table.createIfNotExists();
    }

    private void addDataForm(GridPane gridpane) {
        Label label2 = new Label("Enter Product ID:");
        label2.setFont(new Font(ARIAL_FONT, FONT_SIZE_20));
        
        final TextField prodId = new TextField ();
        HBox hb = new HBox();
        
        hb.getChildren().addAll(label2, prodId);
        hb.setSpacing(10);
        gridpane.add(hb, 0, 11);
        
        Label label3 = new Label("Enter Product Name:");
        label3.setFont(new Font(ARIAL_FONT, FONT_SIZE_20));
        
        final TextField prodName = new TextField ();
        HBox hb2 = new HBox();
        
        hb2.getChildren().addAll(label3, prodName);
        hb2.setSpacing(10);
        gridpane.add(hb2, 0, 12);
        
        Label label4 = new Label("Enter Product Price:");
        label4.setFont(new Font(ARIAL_FONT, FONT_SIZE_20));
        
        final TextField prodPrice = new TextField ();
        HBox hb3 = new HBox();
        
        hb3.getChildren().addAll(label4, prodPrice);
        hb3.setSpacing(10);
        gridpane.add(hb3, 0, 13);
        
        Label label5 = new Label("Enter Product Category:");
        label5.setFont(new Font(ARIAL_FONT, FONT_SIZE_20));
        
        final TextField prodCategory = new TextField ();
        HBox hb4 = new HBox();
        
        hb4.getChildren().addAll(label5, prodCategory);
        hb4.setSpacing(10);
        gridpane.add(hb4, 0, 14);
        
        Button upload = new Button("Add Data!!!");
        GridPane.setConstraints(upload, 0, 15);
        gridpane.getChildren().add(upload);
        
        upload.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(final ActionEvent e) {
                if((prodId.getText() != null && !prodId.getText().isEmpty()) &&
                   (prodName.getText() != null && !prodName.getText().isEmpty()) &&
                   (prodPrice.getText() != null && !prodPrice.getText().isEmpty()) &&
                   (prodCategory.getText() != null && !prodCategory.getText().isEmpty())) {
                    try {
                        addProduct(prodId.getText(), prodName.getText(), prodPrice.getText(), prodCategory.getText());
                    } catch (StorageException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });
    }
    
    //Add Product data to Azure Storage Table
    private void addProduct(String productId, String productName, String productPrice, String productCategory) throws StorageException {
        if(table != null) {
            Product prod = new Product(productId, productName, productCategory, Integer.valueOf(productPrice));
            prod.setRowKey(prod.getProdId()+prod.getProdName());
            prod.setPartitionKey(prod.getCategory());
            
            TableOperation oper = TableOperation.insert(prod);
            
            //Adding Product to Azure Store Table 
            System.out.println("Adding Product to Azure Store Table  ");
            table.execute(oper);
        } else {
            //TODO create a popup to display this error msg
            System.out.println("You need to create table first");
        }
    }
	
    private void listTableContent(GridPane gridpane) {
        Button listBlobs = new Button("List Blobs!!!");
        GridPane.setConstraints(listBlobs, 1, 15);
        gridpane.getChildren().add(listBlobs);

        final ListView listView = new ListView();
        GridPane.setConstraints(listView, 0, 17);
        gridpane.getChildren().add(listView);

        listBlobs.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(final ActionEvent e) {
                listBlobs(listView);
            }
        });
    }
	
    //List Azure table entries
    private void listBlobs(ListView listView) {
        if(table != null) {
            listView.getItems().removeAll();
            TableQuery<Product> query = TableQuery.from(Product.class);
            Iterable<Product> prodList = table.execute(query);
            // Listing contents of container
            for (Product product : prodList) {
                listView.getItems().add(product.getProdId()+"-"+product.getProdName()+"-"+product.getPrice()+"-"+product.getCategory());
            }
        } else {
            //TODO create a popup to display this error msg
            System.out.println("You need to create table first");
        }
    }
	
}
